import java.util.Scanner;
public class Gravitacija {
    public static double gPospesek(double visina){
        double pospesek=6.674*5.972*10000000000000.0/((6.371*1000000.0+visina*1000)*(6.371*1000000.0+visina*1000));
        return pospesek;
    }
    public static void main(String[] args) {
        //System.out.println("OIS je zakon!");
        Scanner sc = new Scanner(System.in);
        System.out.print("Vpiši nadmorsko višino v kilometrih: ");
        double nadmorskaVisina = sc.nextInt();
        izpisi(nadmorskaVisina, gPospesek(nadmorskaVisina));
    }
    public static void izpisi(double nadmorska_visina, double gravitacijski_pospesek){
        System.out.printf("%.2f\n",nadmorska_visina);
        System.out.printf("%.2f\n",gravitacijski_pospesek);
    }
}